from gpiozero import LEDBoard
from gpiozero.tools import random_values
from signal import pause
#tree = LEDBoard(*range(2, 28), pwm=True)
#for led in tree:
#    led.source_delay = 0.1
#    led.source = 2
#pause()

# available pins:  4, 15, 13, 21, 25, 8, 5, 10, 16, 17, 27, 26, 24, 9, 12, 6, 20, 19, 14, 18, 11, 7, 23 and 22
pins = [2, 4, 15, 13, 21, 25, 8, 5, 10, 16, 17, 27, 26, 24, 9, 12, 6, 20, 19, 14, 18, 11, 7, 23, 22]

def blink_led(led_num=2):
    print('blink led', led_num)
    tree =  LEDBoard(led_num, pwm=True)
    tree.blink(on_time=1, off_time=1, fade_in_time=0.5, fade_out_time=0.5, n=None, background=True)
    return True

def led_on(led_num=2):
    print('Turn on led', led_num)
    tree =  LEDBoard(led_num, pwm=True)
    tree.on()
    return True
for number in range(2, 28):
    blink_led(number)

#blink_led(21)
