from gpiozero import LEDBoard
from gpiozero.tools import random_values
from signal import pause

pins = [2, 4, 15, 13, 21, 25, 8, 5, 10, 16, 17, 27,
        26, 24, 9, 12, 6, 20, 19, 14, 18, 11, 7, 23, 22]
tree = LEDBoard(*pins, pwm=True)


def blink_tree(rounds=1):
    print('blink tree')
    tree.blink(on_time=1, off_time=1, fade_in_time=0,
               fade_out_time=0, n=rounds, background=True)
    return 'tree'


def pulse_tree(rounds=1):
    print('blink tree')
    tree.pulse(fade_in_time=1, fade_out_time=1, n=rounds, background=True)
    return 'tree'

def turn_off_led(led_number=None):
    if led_number == None:
        print('Turn off tree.')
        tree.off()
        return 'tree'
    else:
        print('Turn off', led_number)
        tree.off(led_number)
        return led_number


def turn_on_led(led_number=None):
    if led_number == None:
        print('Turn on tree.')
        tree.on()
        return 'tree'
    else:
        print('Turn on', led_number)
        tree.on(led_number)
        return led_number


if __name__ == "__main__":
    blink_tree()
    pause()
