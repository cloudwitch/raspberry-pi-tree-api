from fastapi import FastAPI
import uvicorn

import tree

# initialize API
app = FastAPI()


@app.get('/hello')
async def get_func():
    return {'text': 'hello world'}, 200


@app.get('/tree/blink')
def tree_blink():
    response = tree.blink_tree()
    return {'blink': response}, 200


@app.get('/tree/blink/{rounds}')
def tree_blink(rounds):
    response = tree.blink_tree(int(rounds))
    return {'blink': response}, 200


@app.get('/tree/pulse')
def tree_pulse():
    response = tree.pulse_tree()
    return {'pulse': response}, 200


@app.get('/tree/pulse/{rounds}')
def tree_pulse(rounds):
    response = tree.pulse_tree(int(rounds))
    return {'pulse': response}, 200


@app.get('/tree/on')
def tree_on():
    response = tree.turn_on_led()
    return {'on': response}, 200


@app.get('/tree/on/{led}')
def tree_on(led=2):
    response = tree.turn_on_led(int(led))
    return {'on': response}, 200


@app.get('/tree/off')
def tree_off():
    response = tree.turn_off_led()
    return {'off': response}, 200


@app.get('/tree/off/{led}')
def tree_off(led=2):
    response = tree.turn_off_led(int(led))
    return {'off': response}, 200


@app.get("/items/{item_id}")
async def read_item(item_id):
    return {"item_id": item_id}
