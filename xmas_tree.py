import requests

url = 'http://192.168.2.159:5000'


def hello_world():
    '''
    Return 'hello world'.
    '''
    r = requests.get(f"{url}/hello")
    return r.json()


def blink_once():
    '''
    Blink the tree once.
    '''
    r = requests.get(f"{url}/tree/blink")
    return r.json()


def blink_many(times):
    '''
    Blink the tree number of times.
    '''
    r = requests.get(f"{url}/tree/blink/{times}")
    return r.json()


def pulse():
    '''
    Pulse the tree once.
    '''
    r = requests.get(f"{url}/tree/pulse")
    return r.json()


def pulse_many(times):
    '''
    Pulse the tree number of times.
    '''
    r = requests.get(f"{url}/tree/pulse/{times}")
    return r.json()


def turn_on():
    '''
    Turn the whole tree on.
    '''
    r = requests.get(f"{url}/tree/on")
    return r.json()


def turn_led_on(led):
    '''
    Turn specified LED on.
    '''
    r = requests.get(f"{url}/tree/on/{led}")
    return r.json()


def turn_off():
    '''
    Turn the whole tree off.
    '''
    r = requests.get(f"{url}/tree/off")
    return r.json()


def turn_led_off(led):
    '''
    Turn specified LED off.
    '''
    r = requests.get(f"{url}/tree/off/{led}")
    return r.json()


if __name__ == "__main__":
    print(hello_world())
    print('Blink once.')
    print(blink_once())
    print('Blink 5 times.')
    print(blink_many(5))
    print('Pulse the tree once.')
    print(pulse())
    print('Pulse the tree 5 times.')
    print(pulse_many(5))
    print('Turn the whole tree on.')
    print(turn_on())
    print('Turn the whole tree off.')
    print('Turn on star.')
    print('Turn off star.')
