FROM python:3
COPY requirements.txt /requirements.txt

RUN pip install fastapi gunicorn uvicorn uvloop httptools gpiozero

COPY api.py /api.py
COPY test.py /test.py

ENV WORKERS=2
CMD python /test.py
# CMD gunicorn api:app -w 2 -k uvicorn.workers.UvicornWorker -b "0.0.0.0:80"
